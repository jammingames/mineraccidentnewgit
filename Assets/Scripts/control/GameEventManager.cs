﻿using System.Collections;
using UnityEngine;
using AnimationOrTween;

public static class GameEventManager
{
		public delegate void GameEvent ();

		public static event GameEvent GameStart, GameOver, GameReset, triggerStart, triggerEnd, triggerReset;

		public enum GameState
		{
				Menu,
				Game,
				End,
				Pause}
		;

		public static GameState state = GameState.Menu;

		public static void TriggerGameStart ()
		{
				if (GameStart != null) {
						GameStart ();
						state = GameState.Game;
				}
		}

		public static void TriggerGameOver ()
		{
				if (GameOver != null) {
						GameOver ();
						state = GameState.End;
				}
		}

		public static void TriggerGameReset ()
		{
				if (GameReset != null) {
						GameReset ();
						state = GameState.Menu;
				}
		}

		public static void TriggerStart ()
		{
				if (triggerStart != null) {
						triggerStart ();
				}
		}

		public static void TriggerEnd ()
		{
				if (triggerEnd != null) {
						triggerEnd ();
				}
		}

		public static void TriggerReset ()
		{
				if (triggerReset != null) {
						triggerReset ();
				}
		}
}
