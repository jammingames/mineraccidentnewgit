﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour
{
		//properties
		static GameState _instance = null;

		public static GameState instance {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(GameState)) as GameState;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("GameState");
										_instance = obj.AddComponent<GameState> ();
								}
						}

						return _instance;
				}
		}

		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
				PlayerPrefs.SetInt ("High Score", highScore);
				if (ore > PlayerPrefs.GetInt ("Ore")) {
						PlayerPrefs.SetInt ("Ore", ore);
				}	
		}

		public static int highScore;
		private int highScoreSave;
		private string nextLevel = "level1";
		private string activeLevel;
		private string currentLevel;
		private int activeSkin;
		private int lives;
		private int kills;
		private float maxHP;
		private float hp;
		private int jumpPower;
		private int speed;
		private int toughness;
		private int score;
		private bool busyMon;
		private int tmpOre;
		private int ore;
		private float grav = 1.5f;

		public float getGrav ()
		{
				return grav;
		}

		public void setGrav (float newGrav)
		{
				grav = newGrav;
		}

		private bool angry = false;

		public bool getAngry ()
		{
				return angry;
		}

		public void setAngry (bool newAngry)
		{
				angry = newAngry;
		}

		public bool getBusy ()
		{
				return busyMon;
		}

		public string getLevel ()
		{
				return activeLevel;
		}

		public int getSkin ()
		{
				return activeSkin;
		}

		public int getLives ()
		{
				return lives;
		}

		public int getKills ()
		{
				return kills;
		}

		public float getHP ()
		{
				return hp;
		}

		public float getMaxHP ()
		{
				return maxHP;
		}

		public int getJump ()
		{
				return jumpPower;
		}

		public int getSpeed ()
		{
				return speed;
		}

		public int getToughness ()
		{
				return toughness;
		}

		public int getScore ()
		{
				return score;
		}

		public int getHighScore ()
		{
				highScore = PlayerPrefs.GetInt ("High Score");
				return highScore;
		}

		public int getOre ()
		{ /*ore = PlayerPrefs.GetInt("Ore");*/
				return ore;
		}

		public int getTmpOre ()
		{
				return tmpOre;
		}

		public void setJump (int newJump)
		{
				jumpPower = newJump;
		}

		public void setSpeed (int newSpeed)
		{
				speed = newSpeed;
		}

		public void setToughness (int newTough)
		{
				toughness = newTough;
		}

		public void setBusy (bool newBusy)
		{
				busyMon = newBusy;
		}

		public void  setLevel (string newLevel)
		{
				activeLevel = newLevel;
		}

		public void  setHP (float newHP)
		{
				hp = newHP;
		}

		public void setMaxHP (float newMaxHP)
		{
				maxHP = newMaxHP;
		}

		public void setScore (int newScore)
		{
				score = newScore;
		}

		public void setHighScore (int newHighScore)
		{
				highScore = newHighScore;
		}

		public void setOre (int newOre)
		{
				ore = newOre;
		}

		public void setLives (int newLives)
		{
				lives = newLives;
		}

		public void setKills (int newKills)
		{
				kills = newKills;
		}
		/*
	void  Awake (){
				
				DontDestroyOnLoad(instance);
				}
	*/
		private void  addOre ()
		{
				ore = PlayerPrefs.GetInt ("Ore");
				highScore = PlayerPrefs.GetInt ("High Score");
				int tmpScore = score;
				tmpOre = tmpScore / 11;
				float hsMult = 1.5f;
				if (highScore == 0) {
						highScore = tmpScore;
				}
				if (score > highScore) {
						highScore = score;
						PlayerPrefs.SetInt ("High Score", highScore);
						tmpOre *= (int)hsMult;
				}
				ore += tmpOre;
				PlayerPrefs.SetInt ("Ore", ore);
				//tmp2 = score/3;
				//return tmpOre;
	
		}

		void  Awake ()
		{
			
				
				GameEventManager.GameStart += startGame1;
				GameEventManager.GameOver += gameOver;


		}

		public void  loadCurrent ()
		{
				switch (nextLevel) {
				case "level1":
						startGame1 ();
						break;
				case "level2":
						startGame1 ();
						break;
				case "level3":
						startGame2 ();
						break;
				case "level4":
						startGame3 ();
						break;
				case "level5":
						startGame4 ();
						break;
				case "victory":
						startGame5 ();
						break;
				default:
						startGame1 ();
						break;
				}
		}

		public void  loadNext ()
		{
				switch (nextLevel) {
				case "level1":
						startGame1 ();
						break;
				case "level2":
						startGame2 ();
						break;
				case "level3":
						startGame3 ();
						break;
				case "level4":
						startGame4 ();
						break;
				case "level5":
						startGame5 ();
						break;
				case "victory":
						ObjectReference.instance.gameTweens.OnTriggerReset ();
						break;

				}
		}

		public void startGame1 ()
		{
		

				print ("Starting Level 1");
				highScore = PlayerPrefs.GetInt ("High Score");
				activeLevel = "level1";
				nextLevel = "level2";
				//activeSkin = 0;
				grav = 1;
				lives = 1;
				kills = 0;
				score = 0;
				maxHP = 3;
				hp = 0;
				jumpPower = 25;
				speed = 1;
				toughness = 1;
				//ore = 0;
				//Time.timeScale = 0;
				//Application.LoadLevel ("level1");
				//Time.timeScale = 0;
		}

		public void startGame2 ()
		{
				print ("Creating a new game state");
				highScore = PlayerPrefs.GetInt ("High Score");
				activeLevel = "level2";
				nextLevel = "level3";
				grav = 2;
				lives = 1;
				kills = 0;
				//score;
				maxHP = 4;
				hp = 0;
				jumpPower = 18;
				speed = 1;
				toughness = 1;
				//ore = 0;
				Time.timeScale = 0;
				Application.LoadLevel ("level2");
		}

		public void startGame3 ()
		{
				print ("Creating a new game state");
				highScore = PlayerPrefs.GetInt ("High Score");
				activeLevel = "level3";
				nextLevel = "level4";
				grav = 2.3f;
				lives = 1;
				kills = 0;
				//score;
				maxHP = 5;
				hp = 0;
				jumpPower = 18;
				speed = 1;
				toughness = 1;
				//ore = 0;
				Time.timeScale = 0;
				Application.LoadLevel ("level3");
		}

		public void startGame4 ()
		{
				print ("Creating a new game state");
				highScore = PlayerPrefs.GetInt ("High Score");
				activeLevel = "level4";
				nextLevel = "level5";
				grav = 2.6f;
				lives = 1;
				kills = 0;
				//score;
				maxHP = 5;
				hp = 0;
				jumpPower = 18;
				speed = 1;
				toughness = 1;
				//ore = 0;
				Time.timeScale = 0;
				//Application.LoadLevel ("level4");
		}

		public void startGame5 ()
		{
				print ("Creating a new game state");
				highScore = PlayerPrefs.GetInt ("High Score");
				activeLevel = "level5";
				nextLevel = "victory";
				grav = 3;
				lives = 1;
				kills = 0;
				//score;
				maxHP = 5;
				hp = 0;
				jumpPower = 18;
				speed = 1;
				toughness = 1;
				//ore = 0;
				Time.timeScale = 0;
				Application.LoadLevel ("level5");
		}

		public void gameOver ()
		{
				addOre ();
				print ("Creating a new game state");
		
				activeLevel = "gameover";
				//Application.LoadLevel ("gameover");
		
		}
}
