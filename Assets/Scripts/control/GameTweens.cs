﻿using UnityEngine;
using System.Collections;

public class GameTweens : MonoBehaviour
{
		public Transform menuObj, gameObj, gameOverObj;
		public AudioSource audio;
		private Color col;
		public GameObject fader;
		public SpriteRenderer fadeObj;
		public bool isInTransition = false;
		public bool changePitch = false;
		public bool changePitchDown = false;

		void Awake ()
		{
				GameEventManager.triggerStart += OnGameStart;
				GameEventManager.triggerEnd += OnGameEnd;
				GameEventManager.triggerReset += OnGameRestart;
		}

		void Update ()
		{
//				if (Input.GetKey (KeyCode.V))
//						TriggerStart ();
//				if (Input.GetKey (KeyCode.B))
//						TriggerEnd ();



				if (changePitchDown == true && audio.pitch >= 0.2f) {
						audio.pitch -= 0.02f;

				}
				if (changePitch == true && audio.pitch <= 0.9f) {
						audio.pitch += 0.01f;

				}
		}

		public void OnTriggerStart ()
		{
				GameEventManager.TriggerStart ();
		}

		public void OnTriggerEnd ()
		{
				GameEventManager.TriggerEnd ();
		}

		public void OnTriggerReset ()
		{
				GameEventManager.TriggerReset ();
				
		}

		void OnGameStart ()
		{	
				changePitch = true;
				Debug.Log (changePitch);
				StartCoroutine (fadeObj.material.Fade (1.0f, 0.2f, 0.6f, 
						() => {
				
								StartCoroutine (fadeObj.material.Fade (0.1f, 1.0f, 0.6f, 
										() => {
												GameEventManager.TriggerGameStart ();
										}));
						}));
				StartCoroutine (gameObj.transform.MoveTo (new Vector3 (0, gameObj.position.y, gameObj.position.z), 0.4f, EaseType.CubeIn, () => {
						changePitch = false;
				}));
				
					
				StartCoroutine (menuObj.transform.MoveTo (new Vector3 (-7, menuObj.position.y, menuObj.position.z),
						0.4f,
						EaseType.CubeIn,
						null));
				
		}

		void OnGameEnd ()
		{	
		
				changePitchDown = true;
				StartCoroutine (fadeObj.material.Fade (1.0f, 0, 0.8f, 
						() => {
				
								StartCoroutine (fadeObj.material.Fade (0, 1.0f, 0.8f, null));
								StartCoroutine (gameObj.transform.MoveTo (new Vector3 (-7, gameOverObj.position.y, gameObj.position.z), 0.5f, EaseType.CubeIn, 
										() => {
												GameEventManager.TriggerGameOver ();
										}));
								changePitchDown = false;
						}));
				StartCoroutine (gameOverObj.transform.MoveTo (new Vector3 (0, gameOverObj.position.y, gameOverObj.position.z),
						0.5f,
						EaseType.CubeIn,
						null));
				
		}

		void OnGameRestart ()
		{	

				changePitchDown = true;
				StartCoroutine (fadeObj.material.Fade (1.0f, 0, 0.8f, 
						() => {
				
								StartCoroutine (fadeObj.material.Fade (0, 1.0f, 0.8f, null));
								StartCoroutine (gameOverObj.transform.MoveTo (new Vector3 (-7, gameOverObj.position.y, gameOverObj.position.z), 0.5f, EaseType.CubeIn, 
										() => {
												GameEventManager.TriggerGameReset ();
										}));
								changePitchDown = false;
						}));
				StartCoroutine (menuObj.transform.MoveTo (new Vector3 (0, menuObj.position.y, menuObj.position.z),
						0.5f,
						EaseType.CubeIn,
						null));
		}
}
