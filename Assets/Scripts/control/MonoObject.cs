﻿using UnityEngine;
using System.Collections;

public class MonoObject : MonoBehaviour
{
		// Use this for initialization
		public Vector3 originalPosition;
		public bool resetPosition = true;

		void Awake ()
		{
				GameEventManager.GameStart += OnStart;
				GameEventManager.GameOver += OnEnd;
				GameEventManager.GameReset += OnReset;
				originalPosition = transform.position;
				ObjectReference.instance.gameObjects.Add (this.transform, this.transform.position);
				enabled = false;
		}

		void OnStart ()
		{
				enabled = true;

		}

		void OnEnd ()
		{
				if (this != null) {
						this.transform.position = originalPosition;
						
							
						enabled = false;
				}
		}

		void OnReset ()
		{
		
				
		}

		void OnApplicationQuit ()
		{
				GameEventManager.GameStart -= OnStart;
				GameEventManager.GameOver -= OnEnd;
		}
}
