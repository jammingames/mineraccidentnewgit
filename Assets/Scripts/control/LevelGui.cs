﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class LevelGui : MonoObject
{
		public GUISkin customSkin;
		public static string monState;

		void  OnEnable ()
		{
				//	monState = "Normal";
				print ("Loaded: " + GameState.instance.getLevel ());
				//if ((GameState.instance.getLevel() != "intro") || (GameState.instance.getLevel() != "gameover")) {
				
				
		}

		void  OnGUI ()
		{
				GUI.skin = customSkin;
	
	
	
				switch (GameState.instance.getLevel ()) {
				case "level1":
						if (Time.timeScale == 0) {
								if (GUI.Button (new Rect (70, 150, 200, 200), "Go!")) {
										Time.timeScale = 1;
								}	
						}
						GUI.Label (new Rect (20, 40, 300, 50), "Score: " + GameState.instance.getScore ());	
	//GUI.Label(new Rect(20, 20, 350,50), "Hits Remaining: " + GameState.instance.getHP());	
						GUI.Label (new Rect (50, 0, 300, 50), monState);	
						break;
				case "gameover":
						GUI.Label (new Rect (90, 60, 300, 40), "Score: " + GameState.instance.getScore ());	
						GUI.Label (new Rect (80, 90, 300, 40), "Best Score: " + GameState.instance.getHighScore ());	
						GUI.Label (new Rect (10, 510, 350, 40), "You Earned " + GameState.instance.getTmpOre () + " Miner Ore!");
						if (GUI.Button (new Rect (60, 200, 200, 100), "Main Menu")) {
								print ("Moving to Intro");
								GameState.instance.setLevel ("Intro");
								ObjectReference.instance.gameTweens.OnTriggerReset ();
						}
						if (GUI.Button (new Rect (60, 300, 200, 100), "Try Again?")) {
			
								GameState.instance.setLevel ("level1");
								GameState.instance.startGame1 ();
						}	
						break;
				case "Intro":
	
						break;
				}
				/*
	if (GameState.instance.getLevel() == "Game") {
			
		}
		*/
		}
}
