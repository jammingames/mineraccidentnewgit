﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum States
{
		stDefault,
		idle,
		onPlatform,
		jumping,
		falling,
		stNone,
		wallSliding,
		kickOffWall,
		disabled}
;

public enum Ops
{
		opDefault,
		opNext,
		opContinue,
		opRepeat}
;

public class CharacterControl : MonoObject
{
		/*
The physics has been abstracted into CPhysics class,
that handles how things move, we call that class's methods
 inside of this script to move the character, so we can also
 use it to move platforms for the kirby monster etc 
 */
		/* -- Components -- */
		private Rigidbody playerRigidbody;
		private tk2dAnimatedSprite playerSprite;
		/* -- Physics Properties -- */
		public float gravity = 1.0f;
		private float wallFriction = -0.79f;
		public float wallGravity = -1.5f;
		public float playerAcceleration = 1;
		public float speed = 20;
		public Vector3 playerFriction = new Vector3 (0.01f, 1, 1);
		public Vector3 playerJumpFriction = new Vector3 (1, 0.90f, 1);
		private Vector3 directionVector;
		public float playerJump = 20;
		private int isOnWall;
		private int landed = 0;
		private bool canMove;
		private int wjump = 8;
		private Vector3 movementThisStep;
		private CPhysics pPhys;
		private string side;
		/* -- Raycast Setup -- */
		public LayerMask platformLayer;
		public LayerMask wallLayer;
		private float minimumSize;
		private float partialExtent;
		private float sqrMinimumSize;
		private Vector3 previousPosition;
		/* State Machine Setup */
		public Ops Ops = Ops.opDefault;
		public States States = States.onPlatform;
		public Dictionary<States, System.Func<Ops>> stateTable = new Dictionary<States, System.Func<Ops>> ();
		private StateMachine SM;

		void  Start ()
		{
				GameEventManager.GameStart += OnStartGame;
				GameEventManager.GameOver += OnEndGame;
							
				stateTable.Add (States.stDefault, stDefault);
				stateTable.Add (States.idle, idle);
				stateTable.Add (States.onPlatform, onPlatform);
				stateTable.Add (States.jumping, jumping);
				stateTable.Add (States.falling, falling);
				stateTable.Add (States.wallSliding, wallSliding);
				stateTable.Add (States.kickOffWall, kickOffWall);
				stateTable.Add (States.disabled, disabled);
				
				SM = new StateMachine (Ops, stateTable);

				/* -- Initilize Raycast stuff -- */
				//playerJump = GameState.instance.getJump ();
				//state = SM.getState ();
				playerRigidbody = rigidbody;
				previousPosition = playerRigidbody.position;
				minimumSize = Mathf.Min (collider.bounds.extents.x, collider.bounds.extents.y);
				partialExtent = minimumSize;
				sqrMinimumSize = minimumSize * minimumSize;
				canMove = true;
				playerSprite = GetComponent<tk2dAnimatedSprite> ();
				playerSprite.animationEventDelegate = AnimationEventDelegate;
				pPhys = new CPhysics (transform, platformLayer);
	
		}
		//MUTHAFUCKIN States
		//------------------------------------------------------------------------------------------------
		Ops stDefault ()
		{
				//print ("default");
				SM.setState (States.idle);
				return Ops.opNext;
		}

		Ops  idle ()
		{

				if (!SM.newState ()) {
						////Debug.Log("idle!");
				}
				if (checkLanded ()) {
						//Debug.Log ("COLLIDE!");
						SM.setState (States.onPlatform);
						return Ops.opNext;
				}
				return Ops.opContinue;
		}

		Ops  onPlatform ()
		{ 

				wjump = 3;
				if (SM.newState ()) {
						////Debug.Log("onPlatform!");
						playerSprite.Play ("walkRight");
						canMove = false;
				}
				//if (Input.GetButtonDown("Jump")) {SM.setState(States.jumping); return(Ops.opNext);}
	
				if (SM.getInterrupt (States.jumping)) {
						////Debug.Log("Got Jump Signal!");
						SM.setState (States.jumping);
						SM.clearInterrupt ();
						landed = 0;
						return(Ops.opNext);
				}
				if (SM.getInterrupt (States.disabled)) {
						////Debug.Log("got disable signal");
						SM.setState (States.disabled);
						SM.clearInterrupt ();
						return(Ops.opNext);
				}
	
				//canMove = false;
				return(Ops.opContinue);
		}

		Ops  jumping ()
		{
				if (SM.newState ()) {
						//Debug.Log ("jumping!");
						playerSprite.Play ("jump");
						jump ();
						canMove = true;
				}
				//	if (SM.newState () && checkLanded ()) {
						
				//			//Debug.Log ("----");
				//	}
				if (!SM.newState () && checkFalling ()) {
						////Debug.Log("APEX!");
						SM.setState (States.falling);
						return(Ops.opNext);
				}
				if (SM.getInterrupt (States.disabled)) {
						////Debug.Log("got disable signal");
						SM.setState (States.disabled);
						SM.clearInterrupt ();
						return(Ops.opNext);
				}

				pPhys.doFriction (playerJumpFriction);
				return(Ops.opContinue);
		}

		Ops  falling ()
		{
				
				if (SM.newState ()) {
						//Debug.Log ("FALLING!");
						playerSprite.Play ("falling");
						landed = 0;
						canMove = true;
				}
				if (SM.getInterrupt (States.disabled)) {
						////Debug.Log("got disable signal");
						SM.setState (States.disabled);
						SM.clearInterrupt ();
						return(Ops.opNext);
				}
				if (checkLanded ()) {
						//Debug.Log ("LANDED!");
						playerSprite.Play ("landed");
						SM.setState (States.onPlatform);
						return(Ops.opNext);
				}
	
				if (checkWalls () != 0) {
						//Debug.Log ("hit walls");
						SM.setState (States.wallSliding);
						return(Ops.opNext);
				}

				return(Ops.opContinue);
		}

		Ops  wallSliding ()
		{
				if (SM.newState ()) {
						////Debug.Log("Sliding");
						playerSprite.Play ("wallSlide");
						canMove = false;
						playerSprite.scale.Set (playerSprite.scale.x * isOnWall, playerSprite.scale.y, playerSprite.scale.z);
				}
	
				pPhys.motionSetY (wallGravity);
	
				if (playerRigidbody.velocity.x != 0) {
						pPhys.motionSetX (0);
				}
	
				if (isOnWall == -1 && directionVector.x > 0.2f) {
						if (wjump >= 0) {
								pPhys.motionAdd (new Vector3 (playerJump / 3, playerJump, 0));
								////Debug.Log("Walljump!");
								wjump--; 
								isOnWall = 0;
						} else {
								pPhys.motionAdd (new Vector3 (playerJump / 3, 0, 0));
								wjump--;
						}
						playerSprite.scale = new Vector3 (-1, 1, 1);
						SM.setState (States.kickOffWall);
						return(Ops.opNext);
				}
	
				if (isOnWall == 1 && directionVector.x < -0.2f) {
						if (wjump >= 3) {
								pPhys.motionAdd (new Vector3 (-playerJump / 3, playerJump, 0));
								////Debug.Log("Walljump!");
								isOnWall = 0;
								wjump--;
						} else {
								pPhys.motionAdd (new Vector3 (-playerJump / 3, 0, 0));
								wjump--;
						}
						playerSprite.scale = new Vector3 (1, 1, 1);
						SM.setState (States.kickOffWall);
						return(Ops.opNext);
				}
				if (GameState.instance.getAngry () == true) {
						OnDamage ();
				}
	
				if (checkLanded ()) {
						SM.setState (States.onPlatform);
						return(Ops.opNext);
				}
	
	
				return (Ops.opContinue);
		}

		Ops  kickOffWall ()
		{
				if (SM.newState ()) {
						////Debug.Log("jumping off wall!");
						playerSprite.Play ("wallJump");
						canMove = false;
				}
				if (SM.getInterrupt (States.disabled)) {
						////Debug.Log("disabled");
						SM.clearInterrupt ();
						SM.setState (States.disabled);
						landed = 0;
						return(Ops.opNext);
				}
				if (SM.getInterrupt (States.falling)) {
						////Debug.Log("falling");
						SM.clearInterrupt ();
						SM.setState (States.falling);
						landed = 0;
						return(Ops.opNext);
				} 
				if (checkLanded ()) {
						SM.setState (States.onPlatform);
						return(Ops.opNext);
				}
	
				if (!SM.newState () && checkWalls () != 0 && wjump == 3) {
						SM.setState (States.wallSliding);
						return(Ops.opNext);
				}
	
				if (SM.getInterrupt (States.falling) && checkFalling ()) {
						////Debug.Log("APEX!");
						SM.setState (States.falling);
						return(Ops.opNext);
				}
				pPhys.doFriction (playerJumpFriction);
				return(Ops.opContinue);
		}

		Ops  disabled ()
		{
				if (SM.newState ()) {
						////Debug.Log("OH GOD YOU SUCK");
						playerSprite.Play ("disabled");
						this.collider.isTrigger = true;	
						if (playerRigidbody.velocity.x != 0) {
								pPhys.motionSetX (0);
						}
						canMove = false;
						Time.timeScale = 0.5f;
				}
				if (SM.getInterrupt (States.falling)) {
						SM.setState (States.falling); 
						Time.timeScale = 1;
						return(Ops.opNext);
				}
	
				return(Ops.opContinue);
		}

		Ops  dead ()
		{
				return Ops.opContinue;
		}
		/*------------------------------------------------------------------------------------------------	
								END OF STATE MACHINE 
------------------------------------------------------------------------------------------------	
*/
		void  FixedUpdate ()
		{
				pPhys.doGravity ();
				pPhys.tick ();
		}

		void  Update ()
		{
	
				movementThisStep = playerRigidbody.position - previousPosition;
//	float movementSqrMagnitude = movementThisStep.sqrMagnitude;
				previousPosition = playerRigidbody.position;
				SM.doStates ();
				movePlayer ();
				if (directionVector.x != 0)
						playerSprite.scale = new Vector3 (1 * directionVector.normalized.x, 1, 1);
				if (Input.GetKey (KeyCode.T))
						pPhys.motionAdd (new Vector3 (0, 20, 0));

		}

		void  movePlayer ()
		{
// Vector3 directionVector = Vector3.zero;
				// we assume that device is held parallel to the ground
				// and Home button is in the right hand
        
				// remap device acceleration axis to game coordinates:
				//  1) XY plane of the device is mapped onto XZ plane
				//  2) rotated 90 degrees around Y axis
				//  directionVector.x = Input.acceleration.x;
				//directionVector = Input.acceleration.x;
////Debug.Log(directionVector.x);        
				// clamp acceleration vector to unit sphere
//        if (directionVector.sqrMagnitude > 1)
				//          directionVector.Normalize();
				//DIGITAL CONTROLS FOR PC
				directionVector = new Vector3 (Input.GetAxis ("Horizontal"), 0, 0);
				// ACCELEROMETER CONTROLS FOR MOBILE
				//directionVector = Input.acceleration;
 
				if (directionVector != Vector3.zero) {
						var directionLength = directionVector.magnitude;
						directionVector = directionVector / directionLength;
						directionLength = Mathf.Min (1, directionLength);
						directionLength = directionLength * directionLength;
						// int scale;
        
						directionVector = directionVector * directionLength;
						if (canMove)
								pPhys.motionSetX (directionVector.x * speed);
				}
				if (canMove == true && Time.timeScale > 0) {
						
						
        
						//playerSprite.scale = Vector3(scale,1,1);
						//pPhys.motionSetX (directionVector.x * speed);
						pPhys.motionAdd (new Vector3 (directionVector.x * speed, 0, 0));
			
				}
/*
	if (canMove == true && Time.timeScale > 0) {
		if (Input.GetKey(KeyCode.LeftArrow)) { 
			playerSprite.scale = Vector3(-1,1,1);
			pPhys.motionAdd(Vector3(-.5 - speed * playerAcceleration,0,0));
		}
		if (Input.GetKey(KeyCode.RightArrow)) { 
			playerSprite.scale = Vector3(1,1,1);
			pPhys.motionAdd(Vector3(.5 + speed * playerAcceleration,0,0));
		}
		
	}*/
//	pPhys.doFriction(playerJumpFriction);
	
		}

		void  OnCollisionEnter (Collision collision)
		{
		}

		void  OnCollisionExit (Collision collision)
		{
		}

		void  AnimationEventDelegate (
				tk2dAnimatedSprite sprite,
				tk2dSpriteAnimationClip clip,
				tk2dSpriteAnimationFrame frame,
				int frameNum)
		{
				if (frame.eventInfo == "endRun") {
						SM.setInterrupt (States.jumping);
				} 
				if (frame.eventInfo == "endWallJump") {
						SM.setInterrupt (States.falling);
				} 
				if (frame.eventInfo == "endDisable") {
						SM.setInterrupt (States.falling);
						this.collider.isTrigger = false;
				} 
		}

		void  jump ()
		{
				pPhys.motionAdd (Vector3.up * playerJump);
		}

		bool checkLanded ()
		{
				bool isLanded;
				//wjump = false;
				var pos = playerRigidbody.position - new Vector3 (0, playerRigidbody.collider.bounds.extents.y, 0) + new Vector3 (0,
						          0.1f,
						          0);
				var dist = 0.1f;
				if (Physics.Raycast (pos - new Vector3 (playerRigidbody.collider.bounds.extents.x, 0, 0),
						    Vector3.down,
						    dist,
						    platformLayer) || Physics.Raycast (pos,
						    Vector3.down,
						    dist,
						    platformLayer) || Physics.Raycast (pos + new Vector3 (playerRigidbody.collider.bounds.extents.x,
						    0,
						    0),
						    Vector3.down,
						    dist,
						    platformLayer))
						isLanded = true;
				else
						isLanded = false;
						

				return(isLanded);
		}

		int checkWalls ()
		{

				isOnWall = 0;
				var pos = playerRigidbody.position;
				var dist = collider.bounds.extents.x + 0.1f;
				/*
	if (playerRigidbody.velocity.x < 0) {
	side = "left";
	}
	if (playerRigidbody.velocity.x > 0) {
	side = "right";
	}
	*/ 
				//if (Physics.Raycast(pos, Vector3.left, dist , wallLayer)) { side = "left";}
				//if (Physics.Raycast(pos, Vector3.right, dist , wallLayer)) { side = "right";} 
				Ray ray = new Ray (pos, Vector3.left);
				RaycastHit hitinfo;
				if (Physics.Raycast (ray, out hitinfo, dist, wallLayer)) {
						//Debug.DrawRay (pos, Vector3.left);
						//Debug.Log ("hit left wall");
						isOnWall = -1;
				}
				ray.direction = Vector3.right;
				if (Physics.Raycast (ray, out hitinfo, dist, wallLayer)) {
						//Debug.DrawRay (pos, Vector3.right);
						//Debug.Log ("hit right wall");
						isOnWall = 1;
				}
	 	
				return(isOnWall);
		}

		bool checkFalling ()
		{
				return(movementThisStep.y <= 0.0f);
		}

		void OnDamage ()
		{
				SM.setInterrupt (States.disabled);
				////Debug.Log("You got damaged");
		}

		void  Slow ()
		{
	
		
				////Debug.Log("SLOW!!");
				//Time.timeScale = 0.5f;
				float timer = 2;
	
				if (timer > 0) {
						timer -= Time.deltaTime;
				} else if (timer <= 0) {
						//playerJump += 2;
						//Time.timeScale = 1.0f;
				}
	
		
		}

		void OnStartGame ()
		{
				this.rigidbody.isKinematic = false;
				enabled = true;
		}

		void OnEndGame ()
		{
				this.rigidbody.velocity = Vector3.zero;
				this.rigidbody.isKinematic = true;
				enabled = false;
				
		}

		private State state;
		private State statetest;
}