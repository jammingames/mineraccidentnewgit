﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;
using System;

public class MonsterBehaviour : MonoObject
{
		public enum MonsterState
		{
				NORMAL,
				HIT,
				LURCH,
				GOO,
				ANGRY,
				KIRBY,
				DEAD}
		;

		public int atk = 2;
		public GameObject Particles;
		public GameObject parentObj;
		public GameObject goo;
		public GameObject platforms;
		public GameObject walls;
		public GameObject kparticles;
		public GameTweens tween;
		private GameObject cam;
		public MonsterState monsterState = MonsterState.NORMAL;
		private float health = 100;
		private float max = 100;
		private int deaths;
		private MonsterState nextState = MonsterState.LURCH;
		//private var z
		//private int attackInterval;
		private float timer;
		private tk2dAnimatedSprite anim;
		private GameObject mesh;
		//Function Pointers
		//function normalChangeFunction() = null;
		//function lurchChangeFunction() = null;
		//function gooChangeFunction() = null;
		//function angryChangeFunction() = null;
		//function kirbyChangeFunction() = null;
		//function deadChangeFunction() = null;
		Action normalUpdateFunction;
		Action lurchUpdateFunction;
		Action gooUpdateFunction;
		Action angryUpdateFunction;
		Action kirbyUpdateFunction;
		Action deadUpdateFunction;

		void Start ()
		{

		}

		void  OnEnable ()
		{
				normalUpdateFunction = normalMonsterUpdate;
				lurchUpdateFunction = lurchMonsterUpdate;
				gooUpdateFunction = gooMonsterUpdate;
				angryUpdateFunction = angryMonsterUpdate;
				kirbyUpdateFunction = kirbyMonsterUpdate;

				health = 100;
				max = 100;
				deaths = 0;
				monsterState = MonsterState.NORMAL;
				//anim.Play ("Nomming");
				nextState = MonsterState.LURCH;
				


				LevelGui.monState = "Normal";
				cam = GameObject.FindGameObjectWithTag ("MainCamera");
				mesh = GameObject.Find ("MonsterMesh");
				anim = mesh.GetComponent<tk2dAnimatedSprite> ();
//Debug.Log(anim);
				//resetHP ();
				timer = atk;
//anim = sprite.gameObject.GetComponent<tk2dAnimatedSprite>();
//camera = camera.GetComponent<"SmoothFollow2D">();

				
		}

		void  normalMonsterUpdate ()
		{

				if (health <= 0) {
						this.animation.Play ("change");
						ChangeState (nextState);
						//Debug.Log(nextState);
				}
	
		}

		void lurchMonsterUpdate ()
		{

				//yield WaitForSeconds (3);
	
				//yield return new WaitForSeconds(1);
				if (timer > 0) {
						timer -= Time.deltaTime;
				} else if (timer <= 0) {
						timer = atk;
						lurch ();
				}
	
					
				if (health <= 0) {
				
						
						stopLurch ();
						nextState = MonsterState.GOO;
						animation.Play ("change");
						//Debug.Log(nextState);
				}
		}

		void  gooMonsterUpdate ()
		{

				if (timer > 0) {
						timer -= Time.deltaTime;
				} else if (timer <= 0) {
						timer = atk;
						fireGoo ();
				}
	
		}

		void  angryMonsterUpdate ()
		{
				if (timer < 1.5f) {
						stopSmash ();
				}
				if (timer > 0) {
						timer -= Time.deltaTime;
				} else if (timer <= 0) {
						smash ();
						timer = 3;
	
				}
	
		}

		void  kirbyMonsterUpdate ()
		{
				if (timer > 0) {
						//stopKirby();
						timer -= Time.deltaTime;
				} else if (timer <= 0) {
	
						timer = 3;
						kirby ();
				}
	
		}

		void  normalChangeFunction ()
		{
				resetHP ();
				addKills ();
				//Debug.Log(nextState);
				//parentObj.renderer.material.color = Color.yellow;
		}

		void  ChangeState (MonsterState state)
		{
				monsterState = state;
	
	
				switch (state) {
				case MonsterState.NORMAL:
						anim.Play ("Nomming");
						normalChangeFunction ();
						nextState = MonsterState.LURCH;
						break;
				case MonsterState.LURCH:	
						normalChangeFunction ();
						anim.Play ("yellow");
						LevelGui.monState = "The Lurcher";
						lurch ();
						nextState = MonsterState.GOO;
						break;
				case MonsterState.GOO:
						normalChangeFunction ();
						anim.Play ("green");
						LevelGui.monState = "Gooey Gus";
						nextState = MonsterState.ANGRY;
				
						break;
				case MonsterState.ANGRY:
						normalChangeFunction ();
						anim.Play ("red");
						LevelGui.monState = "Angrysaurus!";
						nextState = MonsterState.KIRBY;
						break;						
				case MonsterState.KIRBY:
						nextState = MonsterState.DEAD;
						normalChangeFunction ();
						anim.Play ("blue");
						LevelGui.monState = "Hoover";
						break;
				case MonsterState.DEAD: 
				//deadChangeFunction();
						this.animation.Play ("dead");
		//		camera.
						break;
				}
				//	this.animation.Play("change");
		
				timer = 3;
	
		}

		void  Update ()
		{
				switch (monsterState) {
				case MonsterState.NORMAL:
						normalUpdateFunction ();
						break;
				case MonsterState.LURCH:
						lurchUpdateFunction ();
						break;
				case MonsterState.GOO:
						gooUpdateFunction ();
						break;
				case MonsterState.ANGRY:
						angryUpdateFunction ();
						break;
				case MonsterState.KIRBY:
						kirbyUpdateFunction ();
						break;
				case MonsterState.DEAD: 
		//	deadUpdateFunction();
						break;
				}
		}

		void change ()
		{
//Debug.Log(nextState);
				GameState.instance.setBusy (false);
				ChangeState (nextState);
//nextState= MonsterState.NORMAL;
		}

		void  OnDamage ()
		{
				Particles.particleSystem.Play ();
				Particles.particleSystem.enableEmission = true;
				health--;
				GameState.instance.setHP (health);
				if (health <= 0) {
						GameState.instance.setBusy (true);
						animation.Stop ();
						animation.PlayQueued ("change", QueueMode.PlayNow);
				}
		}

		void  lurch ()
		{
				
				gameObject.animation.CrossFadeQueued ("lurch");
		}

		void  stopLurch ()
		{

				gameObject.animation.Rewind ("lurch");
				gameObject.animation.Stop ("lurch");

		}

		void  fireGoo ()
		{
				Instantiate (goo, transform.position, Quaternion.Euler (0, 0, UnityEngine.Random.Range (-15, 15)));
		}

		void  smash ()
		{
				walls.SetActive (true);
//walls.particleSystem.Play();
//walls.particleSystem.enableEmission = true;
				GameState.instance.setAngry (true);
				gameObject.animation.Play ("angry");
				cam.gameObject.animation.Play ("cameraShake");
		}

		void  stopSmash ()
		{
				walls.active = false;
				GameState.instance.setAngry (false);
//walls.particleSystem.Stop();	
//walls.particleSystem.enableEmission = false;
				gameObject.animation.Stop ("angry");
				cam.animation.Stop ("cameraShake");

		}

		void  kirby ()
		{
//animate it
				kparticles.active = true;
				//kparticles.particleSystem.enableEmission = true;
				//kparticles.particleSystem.Play();
//				platforms.GetComponent<ManagePlatforms> ().SendMessage ("kirby", null);
		}

		void  stopKirby ()
		{
				kparticles.active = false;
				//kparticles.particleSystem.Stop();
				//kparticles.particleSystem.enableEmission = false;
				//platforms.SendMessage("stopKirby, null");
//				platforms.GetComponent<ManagePlatforms> ().SendMessage ("stopKirby, null");
		}

		void  onDead ()
		{
				//Particles.particleSystem.Play();
				//Particles.particleSystem.enableEmission = true;
				//Time.timeScale = 0.5f;
				GameState.instance.loadNext ();
	
	
		}
		/************************************************************************************************************************
	******************************************************************************************************************************************************
					END GAME TRIGGER
					******************************************************************************************************************************************************
						***************************************************************************************************************************************/
		void  OnTriggerEnter (Collider other)
		{
    
				if (other.gameObject.tag == "Player") {
						//				GameState.instance.gameOver ();
						GameEventManager.TriggerEnd ();
     	
				}
       
		}

		void  addKills ()
		{
				deaths = GameState.instance.getKills ();
				deaths++;
				GameState.instance.setKills (deaths);

		}

		void  resetHP ()
		{
				GameState.instance.setHP (GameState.instance.getMaxHP ());
				health = GameState.instance.getHP ();
		}

		void  victory ()
		{
				Application.LoadLevel ("gameover");
		}
}