﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

//Compartmentalized Physics Class
public class CPhysics : MonoBehaviour
{
		private Rigidbody playerRigidbody;
		private float gravity = GameState.instance.getGrav ();
		private float friction;
		private float airFriction;
		private Vector3 newVelocity;
		private LayerMask platformLayer;
		private Vector3 footingOffset;
		private bool addX = true;
		private bool addY = true;
		private Vector3 previousVelocity;

		public CPhysics (Transform trans, LayerMask platformLayer)
		{
		
				this.playerRigidbody = (GameObject.FindGameObjectWithTag ("Player")).GetComponent<Rigidbody> ();
		
				this.platformLayer = platformLayer;
		
				footingOffset = new Vector3 (playerRigidbody.collider.bounds.extents.x, 0, 0);
		}

		public void  doGravity ()
		{
				newVelocity -= new Vector3 (0, gravity, 0);
		}

		public void  doFriction (Vector3 friction)
		{
				newVelocity = new Vector3 (newVelocity.x * friction.x, newVelocity.y * friction.y, newVelocity.z * friction.z);
		}

		public void  motionAdd (Vector3 vel)
		{
				newVelocity += vel;
		
		}

		public void  motionSetY (float speed)
		{
				newVelocity = new Vector3 (newVelocity.x, speed, newVelocity.z);
				addY = false;
		
		}

		public void  motionSetX (float speed)
		{
				newVelocity = new Vector3 (speed, newVelocity.y, newVelocity.z);
				addX = false;
		
		}

		public void  clampToObject ()
		{
				////Debug.Log("clamping");
				RaycastHit hitInfo;
				if (Physics.Raycast (playerRigidbody.position - footingOffset, Vector3.down, out hitInfo, 1, platformLayer) ||
				    Physics.Raycast (playerRigidbody.position + footingOffset, Vector3.down, out hitInfo, 1, platformLayer)) {
			
						playerRigidbody.position = new Vector3 (playerRigidbody.position.x, hitInfo.point.y + playerRigidbody.collider.bounds.extents.y);
				}
		}

		public void  addPreviousVelocity (bool x, bool y)
		{
		
				if (x) {
						newVelocity += new Vector3 (playerRigidbody.velocity.x, 0, 0);
				}
		
				if (y) { 
						newVelocity += new Vector3 (0, playerRigidbody.velocity.y, 0);
				}
				addX = true;
				addY = true;
		}

		public Vector3 getPreviousVelocity ()
		{
				return(newVelocity);
		}

		public void  tick ()
		{
				previousVelocity = playerRigidbody.velocity;
				addPreviousVelocity (addX, addY);
				newVelocity = new Vector3 (Mathf.Clamp (newVelocity.x, -12, 12), Mathf.Clamp (newVelocity.y, -8, 60), 0);
				playerRigidbody.velocity = newVelocity;
				
				newVelocity = Vector3.zero;
		
		}
}
