﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class Scores : MonoObject
{
		private Rigidbody playerObj;
		//GUIText Score;
		private int playerScore;
		private float tmpScore;
		private int multi;
		// = 1 + gamestate.Instance().getKills();
		private int tmpMulti = 0;

		void  Start ()
		{
				multi = 1;
				playerScore = 1;
				playerObj = ObjectReference.instance.playerRig;
		}

		void  updateScore ()
		{	
				tmpScore = 0;
				if (tmpMulti > 0) {
						tmpScore += tmpMulti;
				}//Debug.Log(tmpMulti);}
				//tmpScore += 1.001f* Time.deltaTime;
				if (playerObj.velocity.y > 0) {
						tmpScore += (int)0.1f * playerObj.velocity.y;
						////Debug.Log(playerObj.rigidbody.velocity.y);
						if (multi > 1) {
								tmpScore *= multi;
						}
						tmpScore *= Time.timeScale;
		
				}
	
				playerScore += (int)tmpScore;
	
				GameState.instance.setScore (playerScore);
				//FIXME_VAR_TYPE sc= gamestate.
//	Debug.Log(gamestate.Instance().getScore());
				tmpMulti = 0;
		}

		void  Update ()
		{
	

				if (GameState.instance.getKills () + 1 > multi) {
						multi += GameState.instance.getKills ();
						tmpMulti = 500;
				}
				//if (Time.timeScale > 0) {
				//Debug.Log(gamestate.Instance().getScore());
				updateScore ();

		
				//}
				////Debug.Log(multi);
		}
}