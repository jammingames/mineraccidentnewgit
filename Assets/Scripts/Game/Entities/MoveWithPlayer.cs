﻿using System.Collections;
using UnityEngine;

public class MoveWithPlayer : MonoObject
{
		protected Rigidbody playerObj;
		protected Transform _tran;
		public float fallRate;

		protected void Move ()
		{
				if (_tran.position.y >= -14)
						_tran.position = new Vector3 (_tran.position.x, (_tran.position.y - (playerObj.velocity.y / fallRate) * Time.deltaTime), _tran.position.z);
						
				
		}
}