﻿using UnityEngine;
using System.Collections;

public class Wall : MoveWithPlayer
{
		Collider thisCollider;
		Vector3 resetPos;
		Vector3 limitPos;

		void Start ()
		{
				_tran = transform;
				playerObj = ObjectReference.instance.playerRig;
				thisCollider = GetComponent<Collider> ();
				resetPos = new Vector3 (_tran.position.x, thisCollider.bounds.extents.y * 2 + _tran.position.y, _tran.position.z);
				limitPos = new Vector3 (resetPos.x, -1 * thisCollider.bounds.extents.y * 2, resetPos.z);
				
		
		}

		void Update ()
		{
				Move ();
				if (_tran.position.y < limitPos.y)
						_tran.position = resetPos;
		}
}
