﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class ScreenBoundaries : MonoBehaviour
{
		Rigidbody playerRigidbody;
		Vector3 col;

		void  Start ()
		{
				playerRigidbody = ObjectReference.instance.playerRig;
				col = playerRigidbody.collider.bounds.extents;
				print (playerRigidbody + "    " + col);
		}

		void  Update ()
		{

		}

		void  OnPreRender ()
		{
				////Debug.Log("Running");
				if (playerRigidbody.position.y - col.y <= -3) {
						Debug.Log ("moving screen up");
						playerRigidbody.position = new Vector3 (playerRigidbody.position.x, -3 + col.y, playerRigidbody.position.z);
						////Debug.Log("go!");
				}
	
				if (playerRigidbody.position.y + col.y >= 3.5f) {
						Debug.Log ("moving screen down");
						playerRigidbody.position = new Vector3 (playerRigidbody.position.x, 3.5f - col.y, playerRigidbody.position.z); // - Mathf.Abs(movementThisStep.y);
				}
	
		}
}