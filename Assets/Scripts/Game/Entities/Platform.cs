﻿using UnityEngine;
using System.Collections;

public class Platform : MoveWithPlayer
{
		Vector3 originalPos;
		Collider thisCollider;
		Vector3 resetPos;
		Vector3 limitPos;
		ManagePlatforms manager;

		void OnEnable ()
		{
				_tran = transform;
				playerObj = ObjectReference.instance.playerRig;
				thisCollider = GetComponentInChildren<Collider> ();
				originalPos = _tran.position;
				resetPos = originalPos;
				//resetPos = new Vector3 (_tran.position.x, thisCollider.bounds.extents.y * 2 + _tran.position.y, _tran.position.z);
				limitPos = new Vector3 (_tran.position.x, -5, _tran.position.z);
				if (manager == null)
						manager = _tran.parent.GetComponent<ManagePlatforms> ();
				
				//print (_tran.localScale.y);
		}

		void Update ()
		{
				Move ();
				if (_tran.position.y < limitPos.y) {
						this.Recycle ();
						manager.platformPool.Remove (this);
						manager.positions.RemoveAt (0);
						manager.positions.Add (manager.positions [manager.positions.Count] + resetPos + Vector3.up);
				}
						
				_tran.position = (new Vector3 (_tran.position.x, _tran.position.y - fallRate / 10 * Time.deltaTime, _tran.position.z));
		}

		void OnDisable ()
		{
				
		}
}
