﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class Bomb : MoveWithPlayer
{
		private bool lit;
		public BoxCollider boxCol;
		public SphereCollider sphereCol;

		void  Start ()
		{
				lit = false;
		}

		void  Update ()
		{
				Move ();
		}

		void  OnTriggerEnter (Collider other)
		{
    
				if (other.gameObject.tag == "Player" && !GameState.instance.getBusy ()) {
						boxCol.isTrigger = true;
						lit = true;
				}
				if (other.gameObject.tag == "Monster" && !GameState.instance.getBusy ()) {
						if (lit == true)
								other.gameObject.SendMessage ("OnDamage", null);
						//this.transform.parent.gameObject.SetActive(false);
						//	this.transform.parent.gameObject.rigidbody.useGravity = false;
						ObjectPool.Recycle (this);
     	
				}
     
		}
}