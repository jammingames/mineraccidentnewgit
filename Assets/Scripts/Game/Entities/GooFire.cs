﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class GooFire : MonoObject
{
		public float power;

		void  Start ()
		{
				power = -14;
				Destroy (this, 3);
    
		}

		void  Update ()
		{
				transform.Translate (new Vector3 (Random.Range (-3, 3), -power, 0) * Time.deltaTime);
   
		}

		void  OnTriggerEnter (Collider other)
		{
				if (other.gameObject.tag == "Player") {
       
						other.gameObject.SendMessage ("OnDamage", true);
						//this.particleSystem.Stop();
						//this.particleSystem.enableEmission = false;
						Destroy (this.gameObject);
						gameObject.active = false;
				}
     	
       
		}
}