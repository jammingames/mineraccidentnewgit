﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;

public class ObjectReference : MonoBehaviour
{
		static ObjectReference _instance = null;

		public static ObjectReference instance {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(ObjectReference)) as ObjectReference;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("ObjectReferences");
										_instance = obj.AddComponent<ObjectReference> ();
								}
						}

						return _instance;
				}
		}

		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}

		public Dictionary<Transform, Vector3> gameObjects;
		public Transform playerTrans;
		public Rigidbody playerRig;
		public MonsterBehaviour monBehaviour;
		public GameTweens gameTweens;
		public Camera cam;
		//public tk2dSprite[]
		void Awake ()
		{
				gameObjects = new Dictionary<Transform, Vector3> ();
				GameEventManager.GameReset += resetPositions;
		}

		public void resetPositions ()
		{
				//foreach (KeyValuePair<Transform, Vector3> item in gameObjects) {

				//		item.Key.position = item.Value;
				//}
		}
}
