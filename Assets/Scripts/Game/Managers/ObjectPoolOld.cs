﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolOld : MonoObject
{
		GameObject gameObjectType;
		List<GameObject> pool;
		Stack available;

		public ObjectPoolOld (GameObject type)
		{
				gameObjectType = type;
				available = new Stack ();
				pool = new List<GameObject> ();
		}

		public GameObject spawn (Vector3 position, int spriteId)
		{
	
				GameObject spawnedObject;
		
				if (available.Count == 0) {
						spawnedObject = GameObject.Instantiate (gameObjectType, position, Quaternion.identity) as GameObject;
						spawnedObject.GetComponent<tk2dSprite> ().spriteId = spriteId;
						pool.Add (spawnedObject);
				} else {
						spawnedObject = available.Pop () as GameObject;
						spawnedObject.GetComponent<tk2dSprite> ().spriteId = spriteId;
						Transform newObjectTransform = spawnedObject.transform;
						newObjectTransform.position = position;
						spawnedObject.active = true;
				}
		
				return spawnedObject;
		}

		public void despawn (GameObject gameObject)
		{
		
				if (!available.Contains (gameObject)) {
						available.Push (gameObject);
						gameObject.active = false;
				}
		
		}

		public void despawnAll ()
		{
		
				for (int count = 0; count < pool.Count; count++) {
						GameObject obj = pool [count] as GameObject;
			
						if (obj.active == true) {
								obj.active = false;
								available.Push (obj);
						}
				}
		}

		public void clearAll ()
		{
				despawnAll ();
				available.Clear ();
				pool.Clear ();
		}

		public void performOnAll (System.Action<GameObject> f, bool activeOnly)
		{
		
				for (int count = 0; count < pool.Count; count++) {
						GameObject obj = pool [count] as GameObject;
			
						if (activeOnly && !obj.active) {
								continue;
						}
			
						f (obj);
			
				}
		
		}
}
