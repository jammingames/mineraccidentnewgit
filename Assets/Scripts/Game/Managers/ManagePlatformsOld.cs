﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ManagePlatformsOld : MonoObject
{
		public GameObject platformPrefab;
		int tick = 0;
		public float fallRate = 1;
		public float fallSpeed = 1.5f;
		public Vector3 onScreen;
		public Vector3 aboveScreen;
		public int randPlatform;
		bool platformTime = false;
		Rigidbody playerObj;
		Transform monster;

		public enum platformSize
		{
				none,
				smallest,
				small,
				medium,
				large
		}

		public platformSize PlatformSize = platformSize.none;
		Hashtable platformType = new Hashtable ();
		ObjectPoolOld platformPool;
		//Holds a platform position and size,
		//used to pass into an array for setting up chunks
		public class PlatformArrayEntry
		{
				public PlatformArrayEntry (Vector2 pos, int size)
				{
						m_nPosition = pos;
						m_nSize	= size;
				}

				public Vector2 m_nPosition;
				public int m_nSize;
		};
		//Platform Chunks
		public static List<PlatformArrayEntry> platformEasyChunk1 = new List<PlatformArrayEntry> ();
		public static List<PlatformArrayEntry> platformEasyChunk2 = new List<PlatformArrayEntry> ();
		//List of Chunks
		List<List<PlatformArrayEntry>> platformChunkList = new List<List<PlatformArrayEntry>> ();
		//Takes in a Chunk : Array, and spawns all platforms with the pool
		void GeneratePlatforms (List<PlatformArrayEntry> chunk, Vector2 pos)
		{
				for (var i = 0; i < chunk.Count; i++) {
						PlatformArrayEntry tObj = chunk [i] as PlatformArrayEntry;
						if (tObj != null) {
								//platformPool.spawn(tObj.m_nPosition + pos, tObj.m_nSize);
								(platformPool.spawn (tObj.m_nPosition + pos, tObj.m_nSize)).transform.parent = transform;
			
			
						}
				}
		}

		void Start ()
		{

				
				GameEventManager.GameOver += onGameEnd;
				//Reference to Player
				playerObj = ObjectReference.instance.playerRig;
				//Reference to Monster
				monster = ObjectReference.instance.monBehaviour.transform;
				
				//fallspeed = gamestate.Instance().getGrav();
				//Set Vector2s for generations
				onScreen = new Vector2 (0, 0);
				aboveScreen = new Vector2 (0, 12);
	
				//Creates a new pool for Platforms
				platformPool = new ObjectPoolOld (platformPrefab.gameObject);
	
				//Caches spriteIDs for different sized platforms
	
				//platformType[platformSize.none] = platformPrefab.GetComponent<tk2dSprite>().GetSpriteIdByName("PlatformNone");
				int none = platformPrefab.GetComponent<tk2dSprite> ().GetSpriteIdByName ("PlatformNone");
				platformType [platformSize.smallest] = platformPrefab.GetComponent<tk2dSprite> ().GetSpriteIdByName ("PlatformSmallest");
				int smallest = platformPrefab.GetComponent<tk2dSprite> ().GetSpriteIdByName ("PlatformSmallest");
				platformType [platformSize.small] = platformPrefab.GetComponent<tk2dSprite> ().GetSpriteIdByName ("PlatformSmall");
				int small = platformPrefab.GetComponent<tk2dSprite> ().GetSpriteIdByName ("PlatformSmall");
				platformType [platformSize.medium] = platformPrefab.GetComponent<tk2dSprite> ().GetSpriteIdByName ("PlatformMedium");
				int medium = platformPrefab.GetComponent<tk2dSprite> ().GetSpriteIdByName ("PlatformMedium");
				platformType [platformSize.large] = platformPrefab.GetComponent<tk2dSprite> ().GetSpriteIdByName ("PlatformLarge");
				int large = platformPrefab.GetComponent<tk2dSprite> ().GetSpriteIdByName ("PlatformLarge");

	
	
				//--------------------------------------------------------------------------------------
				//Creatin' Chunks
				//Platform Easy Chunk 1

				platformEasyChunk1.Add (new PlatformArrayEntry (new Vector2 (0, 0), large));
				platformEasyChunk1.Add (new PlatformArrayEntry (new Vector2 (1, 2), medium));
				platformEasyChunk1.Add (new PlatformArrayEntry (new Vector2 (-2, 3), small));
				platformEasyChunk1.Add (new PlatformArrayEntry (new Vector2 (-1, 5), small));
				platformEasyChunk1.Add (new PlatformArrayEntry (new Vector2 (4, 7), small));
				platformEasyChunk1.Add (new PlatformArrayEntry (new Vector2 (-3, 7), small));
				platformEasyChunk1.Add (new PlatformArrayEntry (new Vector2 (0, 9), medium));
				platformEasyChunk1.Add (new PlatformArrayEntry (new Vector2 (2, 11), large));
				//Platform Chunk 2
				platformEasyChunk2.Add (new PlatformArrayEntry (new Vector2 (0, 0), large));
				platformEasyChunk2.Add (new PlatformArrayEntry (new Vector2 (3, 1), medium));
				platformEasyChunk2.Add (new PlatformArrayEntry (new Vector2 (-1, 3), small));
				platformEasyChunk2.Add (new PlatformArrayEntry (new Vector2 (3, 5), small));
				platformEasyChunk2.Add (new PlatformArrayEntry (new Vector2 (-3.5f, 7), small));
				platformEasyChunk2.Add (new PlatformArrayEntry (new Vector2 (-1, 8), small));
				platformEasyChunk2.Add (new PlatformArrayEntry (new Vector2 (4, 10), medium));
				platformEasyChunk2.Add (new PlatformArrayEntry (new Vector2 (3, 11), medium));
    
				//Push Chunks into Array of Chunks
				platformChunkList.Add (platformEasyChunk1);
				platformChunkList.Add (platformEasyChunk2);
	
				//Spawn starting chunks
				randPlatform = Random.Range (0, 1000) % platformChunkList.Count;
				GeneratePlatforms (platformChunkList [randPlatform], onScreen);
				randPlatform = Random.Range (0, 1000) % platformChunkList.Count;
				GeneratePlatforms (platformChunkList [randPlatform], aboveScreen);
	
				//--------------------------------------------------------------------------------------
		}

		void OnEnable ()
		{
				if (platformChunkList.Count >= 1) {
						randPlatform = Random.Range (0, 1000) % platformChunkList.Count;
						GeneratePlatforms (platformChunkList [randPlatform], onScreen);
						randPlatform = Random.Range (0, 1000) % platformChunkList.Count;
						GeneratePlatforms (platformChunkList [randPlatform], aboveScreen);
				
				} else
						print ("no chunks");
		}

		void Update ()
		{
				if (platformTime == true) { 
						getnewplatforms (); 
						platformTime = false;
				}

		}

		void FixedUpdate ()
		{
				//for every platform
				platformPool.performOnAll (tickPlatforms, true);
				if (tick >= 8) {
						platformTime = true;
				}
                                               
		}

		void getnewplatforms ()
		{

				randPlatform = Random.Range (0, 1000) % platformChunkList.Count;
				GeneratePlatforms (platformChunkList [randPlatform], aboveScreen);
				tick = 0;
		}

		void  tickPlatforms (GameObject childPlatform)
		{

		
				float pos;
				pos = childPlatform.transform.position.y;	
				pos -= (playerObj.velocity.y / fallRate);
				pos -= (fallSpeed * Time.deltaTime);
				if (pos <= monster.transform.position.y) {
						platformPool.despawn (childPlatform);
						tick++;
				}
				childPlatform.transform.position = new Vector3 (childPlatform.transform.position.x, pos, childPlatform.transform.position.z);
		}

		void onGameEnd ()
		{
				platformPool.despawnAll ();
		}
}
