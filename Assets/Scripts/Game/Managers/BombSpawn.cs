﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class BombSpawn : MonoObject
{
		public Bomb bomb;
		public float timer = 5;
		float temptimer;

		void Start ()
		{
				ObjectPool.CreatePool (bomb);
		}

		void  OnEnable ()
		{
				temptimer = timer;
		}

		void  Update ()
		{
				float xRandomRange = Random.Range (0, 6);
				float yRandomRange = Random.Range (12, 24);
				//bawx.transform.localScale = Vector3(Random.Range(3.0f,7.0f),0.5f,0.1f);


	
				if (timer > 0) {
						timer -= Time.deltaTime;
				} else if (timer <= 0) {
						timer = temptimer;
						if (!GameState.instance.getBusy ()) {
								ObjectPool.Spawn (bomb, new Vector3 (xRandomRange, yRandomRange, 0), Quaternion.identity);
						}
	
				}
		}
}