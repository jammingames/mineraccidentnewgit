﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class ManageBackgrounds : MonoObject
{
		public float fallRate = 100;
		private GameObject playerObj;

		void  OnEnable ()
		{
				playerObj = GameObject.FindGameObjectWithTag ("Player");
		}

		void  Update ()
		{
				foreach (Transform child in transform) {
						child.transform.position -= new Vector3 (0, 0.1f * Time.deltaTime, 0);
						child.transform.position -= new Vector3 (0, (playerObj.rigidbody.velocity.y / fallRate) * Time.deltaTime, 0);
						if (child.transform.position.y <= -9.3f) {
								child.transform.position = new Vector3 (transform.position.x, 18.95f, 0);
						}
				}
		}
}