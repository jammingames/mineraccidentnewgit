﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ManageWalls : MonoObject
{
		public Wall wallPrefab;
		public List<Vector3> positions;

		void  OnEnable ()
		{
				ObjectPool.CreatePool (wallPrefab);
				
				
				

	
				
				
				foreach (Vector3 pos in positions) {
						var wallInstance = ObjectPool.Spawn (wallPrefab, pos);
						wallInstance.gameObject.SetParent (this.gameObject);
						wallInstance.enabled = true;
				}
				
				
		}
}