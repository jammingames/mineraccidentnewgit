﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System;
using System.Collections;

public class State
{
		string name;
		Action action;
		bool defaultState;
		Hashtable events = new Hashtable ();

		State (String name, Action action, bool defaultState)
		{
				this.name = name;
				this.action = action;
				this.defaultState = defaultState;
		}

		public void  addEvent (String name, Action action)
		{
				events.Add (name, action);
		}
}
