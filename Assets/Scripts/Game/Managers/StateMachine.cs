﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StateMachine
{
		private Ops operation;
		private States currentState;
		private States nextState;
		private States previousState;
		private States defaultState;
		private States interrupts;
		private Dictionary<States, System.Func<Ops>> stateTable = new Dictionary<States, System.Func<Ops>> ();

		public StateMachine (Ops defaultOp, Dictionary<States, System.Func<Ops>> table)
		{
				operation = defaultOp;
				stateTable = table;
				defaultState = States.stDefault;
				previousState = defaultState;
		
		}

		public void  doStates ()
		{
				var stateToDo = stateTable [defaultState] as Func<Ops>;
		
				switch (operation) {
		
				case Ops.opDefault:
						stateToDo = stateTable [defaultState];
						currentState = defaultState;
						break;
				
				case Ops.opContinue:
						previousState = currentState;
						stateToDo = stateTable [currentState];
						break;
				
				case Ops.opNext:
						currentState = nextState;
						stateToDo = stateTable [nextState];
						break;
				
				}
		
				operation = stateToDo ();
		
		}

		public States  getState ()
		{
				return currentState;
		}

		public void  setState (States state)
		{
				nextState = state;
		}

		public States  getPrevious ()
		{
				return previousState;
		}

		public States  getNext ()
		{
				return nextState;
		}

		public bool  newState ()
		{
				return(getPrevious () != getState ());
		}

		public void  setInterrupt (States stateName)
		{
				interrupts = stateName;
		}

		public bool  getInterrupt (States stateName)
		{
				//eventually set this to on in a bitwise flag enum
				return(interrupts == stateName);
		}

		public void  clearInterrupt ()
		{
				//eventually set this to off in a bitwise flag enum
				interrupts = States.stNone;
		}
};
